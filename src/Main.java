
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.sparql.pfunction.PropertyFunctionRegistry;
import org.apache.jena.query.* ;

import exploitationManager.AfterPropertyFunction;
import exploitationManager.BeforePropertyFunction;
import exploitationManager.EpisodeAfterFactorPropertyFunction;
import exploitationManager.EpisodeAtFactorDatePropertyFunction;
import exploitationManager.EpisodeBeforeFactorPropertyFunction;
import exploitationManager.IsFactorEnterInPropertyFunction;
import exploitationManager.IsFactorOfExplainedEventTypePropertyFunction;
import exploitationManager.IsFactorOfExplainedTrajectoryTypePropertyFunction;
import exploitationManager.IsFactorOfExplainingEventTypePropertyFunction;
import exploitationManager.IsFactorOfExplainingTrajectoryTypePropertyFunction;
import exploitationManager.IsFactorOutFromPropertyFunction;
import exploitationManager.OverlapsPropertyFunction;


public class Main { 	 

	public static void main(String[] args) throws IOException {
		
		// Using the Exploitation Manager API functions
		
		// Create a Jena Model
	    Model model = ModelFactory.createDefaultModel();
	    
	    // load the lto Ontology and the test data set 
	    model.read(new FileInputStream("data/data.ttl"), null, "TTL");
	    
	    //Put the proposed functions in the registry
	    final PropertyFunctionRegistry reg = PropertyFunctionRegistry.chooseRegistry(ARQ.getContext());
	    reg.put("http://purl.org/ltofn#before", new BeforePropertyFunction());
	    reg.put("http://purl.org/ltofn#after", new AfterPropertyFunction());
	    reg.put("http://purl.org/ltofn#overlaps", new OverlapsPropertyFunction());
	    reg.put("http://purl.org/ltofn#isFactorEnterIn", new IsFactorEnterInPropertyFunction());
	    reg.put("http://purl.org/ltofn#isFactorOutFrom", new IsFactorOutFromPropertyFunction());
	    reg.put("http://purl.org/ltofn#episodeAfterFactor", new EpisodeAfterFactorPropertyFunction());
	    reg.put("http://purl.org/ltofn#episodeBeforeFactor", new EpisodeBeforeFactorPropertyFunction());
	    reg.put("http://purl.org/ltofn#episodeAtFactorDate", new EpisodeAtFactorDatePropertyFunction());
	    reg.put("http://purl.org/ltofn#isFactorOfExplainedEventType", new IsFactorOfExplainedEventTypePropertyFunction());
	    reg.put("http://purl.org/ltofn#isFactorOfExplainedTrajectoryType", new IsFactorOfExplainedTrajectoryTypePropertyFunction());
	    reg.put("http://purl.org/ltofn#isFactorOfExplainingEventType", new IsFactorOfExplainingEventTypePropertyFunction());
	    reg.put("http://purl.org/ltofn#isFactorOfExplainingTrajectoryType", new IsFactorOfExplainingTrajectoryTypePropertyFunction());

	    PropertyFunctionRegistry.set(ARQ.getContext(), reg);
	   		
	    // These queries can be used to test the proposed functions
		String queryStringTest1 = "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX geofla: <http://data.ign.fr/def/geofla#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ " PREFIX owl: <https://www.w3.org/TR/owl-time/#> "
				+ "SELECT ?residentialTrajectory WHERE { ?residentialTrajectory rdfs:type lto:ResidentialTrajectory . "
				+  "?residentialTrajectory lto:hasResidentialEpisode ?episode1 . ?residentialTrajectory lto:hasResidentialEpisode ?episode2 ."
				+ "{?episode1 lto:isIn ?canton .} UNION {?episode1 lto:isIn ?commune . ?commune geofla:cant ?canton .}"
				+ "?canton rdfs:label 'OISANS ROMANCHE'@fr . ?episode2 lto:isIn ?commune1 . ?commune1 rdfs:label 'GRENOBLE'@fr  ."
				+ "?episode2 ltofn:overlaps ?episode1}";
		
		String queryStringTest2 =  "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX geofla: <http://data.ign.fr/def/geofla#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ "PREFIX time: <https://www.w3.org/TR/owl-time/#> "
				+ "SELECT ?explanatoryFactor "
				+ "WHERE { ?explanatoryFactor rdfs:type lto:ExplanatoryFactor ."
				+ "?commune1 rdfs:label \"GRENOBLE\"@fr ."
				+ "?explanatoryFactor ltofn:isFactorOutFrom ?commune1 ."
				+ "?commune2 rdfs:label \"SAINT-MARTIN D'URIAGE\"@fr . "
				+ "?explanatoryFactor ltofn:isFactorEnterIn ?commune2 . }";
				
		String queryStringTest3 =  "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ "PREFIX time: <https://www.w3.org/TR/owl-time/#> "
				+ "SELECT ?professionalEpisode ?residentialEpisodeBefore ?explanatoryFactor ?residentialEpisodeAfter "
				+ "WHERE { ?explanatoryFactor rdfs:type lto:ExplanatoryFactor ."
				+ "?commune1 rdfs:label \"GRENOBLE\"@fr ."
				+ "?explanatoryFactor ltofn:isFactorOutFrom ?commune1 ."
				+ "?commune2 rdfs:label \"SAINT-MARTIN D'URIAGE\"@fr ."
				+ "?explanatoryFactor ltofn:isFactorEnterIn ?commune2 ."
				+ "?residentialEpisodeAfter rdfs:type lto:ResidentialEpisode ."
				+ "?residentialEpisodeAfter ltofn:episodeAfterFactor ?explanatoryFactor . "
				+ "?residentialEpisodeBefore rdfs:type lto:ResidentialEpisode . "
				+ "?residentialEpisodeBefore ltofn:episodeBeforeFactor ?explanatoryFactor ."
				+ "?professionalEpisode rdfs:type lto:ProfessionalEpisode ."
				+ "?professionalEpisode ltofn:episodeAtFactorDate ?explanatoryFactor .}";	
		
		String queryStringTest4 = "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ "SELECT ?explanatoryFactor "
				+ "WHERE { ?explanatoryFactor rdfs:type lto:ExplanatoryFactor . "
				+ "?explanatoryFactor ltofn:isFactorOfExplainedEventType lto:residentialEventMove .}";
		
		String queryStringTest5 = "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ "SELECT ?explanatoryFactor "
				+ "WHERE { ?explanatoryFactor rdfs:type lto:ExplanatoryFactor . "
				+ "?explanatoryFactor ltofn:isFactorOfExplainedTrajectoryType lto:ResidentialTrajectory .}";
		
		String queryStringTest6 = "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ "SELECT ?explanatoryFactor "
				+ "WHERE { ?explanatoryFactor rdfs:type lto:ExplanatoryFactor . "
				+ "?explanatoryFactor ltofn:isFactorOfExplainingEventType lto:professionalEventChange .}";

		String queryString = "PREFIX ltofn: <http://purl.org/ltofn#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX lto: <http://www.purl.org/lto#> "
				+ "SELECT ?explanatoryFactor "
				+ "WHERE { ?explanatoryFactor rdfs:type lto:ExplanatoryFactor . "
				+ "?explanatoryFactor ltofn:isFactorOfExplainingTrajectoryType lto:ProfessionalTrajectory .}";

		Query query = QueryFactory.create(queryString) ;
		  
		try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
			 
		    ResultSet results = qexec.execSelect() ;
		    
		    for ( ; results.hasNext() ; )
		    {
		      QuerySolution soln = results.nextSolution() ;
		      System.out.println(soln.toString());
		     
		      // Get the binded resource or litteral
		      //Resource r = soln.getResource("") ;
		      // Literal l = soln.getLiteral("") ;
		      }
		  }
	}
}
